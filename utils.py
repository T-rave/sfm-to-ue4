import re, os

def get_version(file_path):
    match = re.findall(r'v\d+$', os.path.splitext(file_path)[0])
    if match:
        return match[0]
