import re
import unreal
import os
import json
from glob import glob
from . import utils


class ShotAssembler:

    def __init__(self, root_dir=None):
        self.FPS = unreal.FrameRate(24)
        self.ACTOR_SCALE_FACTOR = 1

        self.asset_tools = unreal.AssetToolsHelpers.get_asset_tools()
        self.asset_library = unreal.EditorAssetLibrary()
        self.section_extensions = unreal.MovieSceneSectionExtensions()

        if root_dir is None:
            from tkinter import Tk
            from tkinter import filedialog
            tk_root = Tk()
            tk_root.withdraw()
            root_dir = filedialog.askdirectory(initialdir=r'T:\_SIDE-PROJECTS\Apex\example_sequence_root',
                                                        title='Choose your Sequence Root directory')

        self.root_dir = root_dir
        # self.asset_tools.open_editor_for_assets(assets=[self.master_sequence])

    def get_alembics_for_shot(self, sequence, shot):
        found_alembic_files = []
        for root, dir_names, filenames in os.walk(os.path.join(self.root_dir, sequence, shot, 'alembics')):
            for alembic_type_dir in dir_names:
                for sub_root, sub_dirs, sub_filenames in os.walk(root):
                    alembic_files = [f for f in sub_filenames if f.lower().endswith('.abc')]
                    if not alembic_files:
                        continue
                    latest_version = sorted(alembic_files, key=utils.get_version)[-1]
                    full_path_to_alembic = os.path.join(sub_root, latest_version)
                    if full_path_to_alembic in found_alembic_files:
                        continue
                    found_alembic_files.append(full_path_to_alembic)
        return found_alembic_files

    @staticmethod
    def import_alembic(abc_shot_paths, asset_path=None):
        """
        Imports alembics from given paths

        Parameters
        ----------
        abc_shot_paths : list
        asset_path : str

        Returns
        -------

        """
        import_tasks = []

        for abc_path in abc_shot_paths:
            # Create an import task.
            import_task = unreal.AssetImportTask()

            # Set base properties on the task.
            import_task.filename = abc_path
            import_task.destination_path = asset_path or "/Game/Alembics"
            # import_task.destination_name = os.path.splitext(os.path.basename(asset_path))[0] if asset_path\
            #    else os.path.splitext(os.path.basename(abc_path))[0]
            import_task.automated = True  # Suppress UI.

            options = unreal.AbcImportSettings()
            options.import_type = unreal.AlembicImportType.GEOMETRY_CACHE
            options.compression_settings = unreal.AbcCompressionSettings(merge_meshes=True, bake_matrix_animation=True,
                                                                         base_calculation_type=unreal.BaseCalculationType.PERCENTAGE_BASED,
                                                                         percentage_of_total_bases=100.0,
                                                                         max_number_of_bases=0,
                                                                         minimum_number_of_vertex_influence_percentage=0.0)
            options.conversion_settings = unreal.AbcConversionSettings(preset=unreal.AbcConversionPreset.MAYA,
                                                                       flip_u=False, flip_v=True,
                                                                       scale=[100, -100, 100],
                                                                       rotation=[90, 0.0, 0.0])
            options.sampling_settings = unreal.AbcSamplingSettings(sampling_type=unreal.AlembicSamplingType.PER_FRAME,
                                                                   frame_steps=1, time_steps=0.0,
                                                                   # frame_start=1, frame_end=120,
                                                                   skip_empty=False)
            options.material_settings = unreal.AbcMaterialSettings(create_materials=False, find_materials=True)
            import_task.options = options

            import_tasks.append(import_task)

        asset_tools = unreal.AssetToolsHelpers.get_asset_tools()
        asset_tools.import_asset_tasks(import_tasks)

        if not any(t.get_editor_property('imported_object_paths') for t in import_tasks):
            unreal.log_warning('No assets were imported.')

    def get_asset_dirs(self, directory=None):
        if directory is None:
            directory = '/Game/Sequences/{}'.format(self.current_sequence_code)
        if not self.asset_library.does_directory_have_assets(directory, recursive=True):
            return []
        asset_list = self.asset_library.list_assets(directory, recursive=True, include_folder=True)
        return sorted([a for a in asset_list if self.current_sequence_code in a])

    def get_master_sequence(self, sequence):
        master_path = f'/Game/Sequences/{sequence}'
        master_name = f'{sequence}_master_sequence'
        asset_full_path = os.path.join(master_path, master_name)

        if self.asset_library.does_asset_exist(asset_full_path):
            master_sequence = self.asset_library.load_asset(asset_full_path)
        else:
            master_sequence = self.asset_tools.create_asset(asset_name=master_name,
                                                            package_path=master_path,
                                                            asset_class=unreal.LevelSequence,
                                                            factory=unreal.LevelSequenceFactoryNew())
            master_sequence.set_display_rate(self.FPS)
            master_sequence.add_master_track(unreal.MovieSceneCinematicShotTrack)
            self.asset_library.save_asset('{path}/{name}.{name}'.format(path=master_path, name=master_name))

        return master_sequence

    def create_shot(self, sequence, shot_name, start_frame, end_frame, delta=0):
        sequence_asset = self.asset_tools.create_asset(asset_name=shot_name,
                                                       package_path='/Game/Sequences/{}'.format(
                                                           sequence),
                                                       asset_class=unreal.LevelSequence,
                                                       factory=unreal.LevelSequenceFactoryNew())
        sequence_asset.set_display_rate(self.FPS)
        sequence_asset.set_playback_start(start_frame)
        sequence_asset.set_playback_end(end_frame)
        sequence_asset.set_work_range_start(start_frame)
        sequence_asset.set_work_range_end(end_frame)
        sequence_asset.set_view_range_start(start_frame)
        sequence_asset.set_view_range_end(end_frame)

        # add MovieSceneCinematicShotTrack track to your master_sequence
        master_sequence = self.get_master_sequence(sequence)
        shot_track = master_sequence.get_master_tracks()[0]
        # add a section to the track
        section = shot_track.add_section()
        # add your shot sequence to the section
        section.set_editor_property('sub_sequence', sequence_asset)
        section.set_shot_display_name(shot_name)
        self.section_extensions.set_range(section, start_frame + delta, end_frame + delta)

        return sequence_asset, section

    @staticmethod
    def import_cam_fbx(camera_fbx_path, shot_asset, master_sequence=None):
        # get the world
        world = unreal.EditorLevelLibrary.get_editor_world()
        camera_name = "{}_camera".format(shot_asset.get_name())
        # Create Spawnable Camera
        binding = shot_asset.add_spawnable_from_class(unreal.CineCameraActor)
        binding.set_name(camera_name)

        # Set Camera Actor Label
        bound_objects = unreal.SequencerTools.get_bound_objects(world, shot_asset, [binding],
                                                                shot_asset.get_playback_range())
        camera_actor = bound_objects[0].bound_objects[0]
        camera_actor.set_actor_label(camera_name)

        # Create Camera Cut Track
        camera_cut_track = shot_asset.add_master_track(unreal.MovieSceneCameraCutTrack)
        camera_section = camera_cut_track.add_section()
        camera_section.set_range(shot_asset.get_playback_start(), shot_asset.get_playback_end())

        # Attach Camera to Cut Track
        camera_id = master_sequence.make_binding_id(binding)
        camera_section.set_camera_binding_id(camera_id)

        # Set Options
        import_options = unreal.MovieSceneUserImportFBXSettings()
        import_options.set_editor_property('reduce_keys', False)
        import_options.set_editor_property('create_cameras', False)
        import_options.set_editor_property('match_by_name_only', True)

        # Import
        unreal.SequencerTools.import_fbx(world, shot_asset, [binding], import_options, camera_fbx_path)

        # Set Camera Filmback Settings
        camera_comp_binding = binding.get_child_possessables()[0]
        filmback_width_track = camera_comp_binding.add_track(unreal.MovieSceneFloatTrack)
        filmback_width_track.set_property_name_and_path("SensorWidth", "Filmback.SensorWidth")
        filmback_width_section = filmback_width_track.add_section()
        filmback_width_section.set_range(0, 313)
        filmback_width_channel = filmback_width_section.get_channels()[0]
        filmback_width_channel.set_default(48.1585)
        filmback_height_track = camera_comp_binding.add_track(unreal.MovieSceneFloatTrack)
        filmback_height_track.set_property_name_and_path("SensorHeight", "Filmback.SensorHeight")
        filmback_height_section = filmback_height_track.add_section()
        filmback_height_section.set_range(0, 313)
        filmback_height_channel = filmback_height_section.get_channels()[0]
        filmback_height_channel.set_default(20.15)

    def get_latest_cam(self, shot_dir):
        found_camera_fbxes = []
        for root, dir_names, filenames in os.walk(shot_dir):
            found_camera_fbxes.extend(glob(os.path.join(root, 'camera', '*.fbx')))
        latest_cam = sorted(found_camera_fbxes, key=utils.get_version)[-1]
        return latest_cam

    def get_latest_metadata(self, shot_dir):
        meta_files = os.listdir(os.path.join(shot_dir, 'metadata'))
        if len(meta_files) == 0:
            return None
        latest_meta = sorted(meta_files, key=utils.get_version)[-1]
        latest_meta_path = os.path.join(shot_dir, 'metadata', latest_meta)
        with open(latest_meta_path, 'r') as f:
            meta = json.load(f)
            return meta

    def import_shot(self, sequence_name, shot_code):
        current_shot_dir = os.path.join(self.root_dir, sequence_name, shot_code)
        shot_metadata = self.get_latest_metadata(current_shot_dir)
        if not shot_metadata:
            return

        start_frame = int(shot_metadata.get("start_frame", 0))
        end_frame = int(shot_metadata.get("end_frame", 0))
        delta = int(shot_metadata.get("delta", 0))

        # create shot
        shot_asset, shot_section = self.create_shot(sequence_name, shot_code, start_frame, end_frame, delta=delta)
        # Begin camera discovery & import them
        fbx_cam_path = self.get_latest_cam(current_shot_dir)
        master_sequence = self.get_master_sequence(sequence_name)
        self.import_cam_fbx(camera_fbx_path=fbx_cam_path, shot_asset=shot_asset, master_sequence=master_sequence)

        shot_alembic_files = self.get_alembics_for_shot(sequence=sequence_name, shot=shot_code)

        if not shot_alembic_files:
            return

        for shot_alembic in shot_alembic_files:
            self.import_alembic(abc_shot_paths=[shot_alembic])
            alembic_ue4_path = "/Game/Alembics/{}".format(os.path.splitext(os.path.basename(shot_alembic))[0])
            asset = unreal.AssetRegistryHelpers.get_asset_registry().get_asset_by_object_path(alembic_ue4_path)
            geometry_cache = unreal.EditorAssetLibrary.load_asset(asset.get_full_name())
            # actor = unreal.EditorLevelLibrary.spawn_actor_from_object(geometry_cache, location=unreal.Vector(0, 0, 0))

            # actor.set_actor_scale3d(unreal.Vector(ACTOR_SCALE_FACTOR, ACTOR_SCALE_FACTOR, ACTOR_SCALE_FACTOR))

            # add geo cache actor to shot
            spawnable = shot_asset.add_spawnable_from_instance(geometry_cache)
            # add geo cache track to possessable
            animation_track = spawnable.add_track(unreal.MovieSceneGeometryCacheTrack)
            # add section
            animation_section = animation_track.add_section()
            # set section's range
            animation_section.set_range(0, geometry_cache.end_frame)

            # assign the actual geo cache asset to the section
            params = unreal.MovieSceneGeometryCacheParams()
            params.set_editor_property('geometry_cache_asset', geometry_cache)
            animation_section.set_editor_property('Params', params)

        """
        master_sequence = self.get_master_sequence(sequence_name)
        master_sequence.set_playback_start(0)
        master_sequence.set_playback_end(last_end_frame)
        master_sequence.set_work_range_start(0)
        master_sequence.set_work_range_end(last_end_frame)
        master_sequence.set_view_range_start(0)
        master_sequence.set_view_range_end(last_end_frame)
        """


# Example usage:
# sa = ShotAssembler()
# sa.import_shot('SF_0010')
