from . import ui
from PySide2.QtWidgets import QApplication
import sys, os, json

app = None
mainWindow = None

def main():
    global app
    global mainWindow

    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()

    with open(os.path.join(os.path.dirname(__file__), 'Obit.qss'), 'r') as f:
        app.setStyleSheet(f.read())
    mainWindow = ui.ShotAssemblerMainWindow()
    mainWindow.show()

if __name__ == "__main__":
    main()
